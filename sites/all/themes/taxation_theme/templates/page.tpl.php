<!DOCTYPE html>
<html lang="<?php echo $language->language ?>" dir="<?php echo $language->dir ?>">
    <head>
        <?php print $head ?>
        <title><?php print $head_title ?></title>
        <?php print $styles ?>
        <?php print $scripts ?>
        <!--[if lt IE 9]>
          <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
    </head>

    <body class="<?php print $body_classes; ?><?php print phptemplate_body_class($middle, $right); ?>">

        <div id="wrapper" class="clearfix">

           <?php include_once('header.tpl.php'); ?>
            
            
            <div id="content">
                <div class="left-col">
                    <!-- /#main -->
                    
                        <aside id="sidebar-left" role="complimentary" class="sidebar clearfix">
                           
                            <?php if (!empty($messages)): print $messages;
                endif; ?>
                <?php if (!empty($mission)): ?><div id="mission"><?php print $mission; ?></div><?php endif; ?>
                  <?php if (!empty($tabs)): ?><div class="tabs-wrapper clearfix"><?php print $tabs; ?></div><?php endif; ?>
<?php if (!empty($help)): print $help;
endif; ?>            
                <?php if (!empty($title)): ?><div id="page-heading"><h1 class="title" id="page-title"><?php print $title ?></h1></div><?php endif; ?>

                <?php print $content ; ?>
                <?php print $inner_content ; ?>
               
                            
                        </aside> <!-- /sidebar-left -->
                           
                        <?php if (!empty($middle)): ?>
                        <section id="main" role="main" class="clearfix">
                            <?php print $middle; ?>
                        </section>
                               <?php endif; ?>
                        <div class="clear"></div>
                    </div>
               

                <?php if (!empty($right)): ?>
                    <aside id="sidebar-right" role="complimentary" class="sidebar clearfix">
                        <?php print $right; ?>
                    </aside> <!-- /sidebar-right -->
                <?php endif; ?>                
                <div class="clear"></div>
            </div>
        
            

    <?php include_once('footer.tpl.php'); ?>
            <?php print $closure ?>
        </div> <!-- /#wrapper -->

        
        
    </body>
</html>