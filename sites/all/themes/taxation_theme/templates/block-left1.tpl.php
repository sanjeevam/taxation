<section  id="block-<?php print $block->module .'-'. $block->delta; ?>" class="block-left block block-<?php print $block->module ?> clearfix">

  <?php if (!empty($block->subject)): ?>
   <div class="heading-outer"><h2><a href="#"><?php print $block->subject ?></a></h2></div>
  <?php endif;?>

  <div class="content">
    <?php print $edit_links; ?>
    <?php print $block->content ?>
  </div>

</section> <!-- /.block -->
