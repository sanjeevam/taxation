            <header id="header" role="banner" class="clearfix">
                <div class="head" >
                    <div class="logo-container">
                        <?php if ($logo): ?>
                            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" id="logo">
                                <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
                            </a>
                        <?php endif; ?>

                        <?php //if ($site_slogan): ?>
                            <div id="site-name-slogan">
                                <?php if ($site_slogan): ?>
                                    <div id="site-slogan"><?php print $site_slogan; ?></div>
                                <?php endif; ?>
                            </div>
                        <?php //endif; ?>
                    </div>

                    <div class="search-box">
                        <?php if (!empty($head_banner)): ?>
                            <div class="banner-top-img">
                                <?php print $head_banner; ?>
                            </div> <!-- /sidebar-right -->
                        <?php endif; ?>
                        <div class="search-outer"><?php if ($search_box): ?><?php print $search_box ?><?php endif; ?></div>
                    </div>

                </div>


                <div class="nav-right-link"><div><a href="http://www.taxation-jobs.co.uk">Jobs</a> <a href="#">Subscribe</a></div></div>
                <nav id="navigation" role="navigation" class="clearfix for-menu" >
                    <?php if ($navigation): ?>
                        <?php print $navigation ?>
                    <?php endif; ?>
                </nav> <!-- /#navigation -->



                <div class="bread">
                    <div class="top_date"><?php echo format_date(time(), 'large'); ?>
                    </div>
                    <?php if (!empty($breadcrumb)): ?>
                        <div class="top_breadcrmb"><?php print $breadcrumb; ?></div>
                    <?php endif; ?>
                </div>
            </header> <!-- /#header -->