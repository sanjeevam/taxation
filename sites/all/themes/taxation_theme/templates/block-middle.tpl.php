<section id="block-<?php print $block->module .'-'. $block->delta; ?>" class="block-middle block block-<?php print $block->module ?> clearfix">

  <?php if (!empty($block->subject)): ?>
    <div class="heading-outer"><h2><?php print $block->subject ?></h2></div>
  <?php endif;?>

  <div class="content box-line">
      <div class="box-line-top">
      <div class="box-line-bottom">
    <?php print $edit_links; ?>
    <?php print $block->content ?>
    </div>
  </div>
  </div>

</section> <!-- /.block -->
