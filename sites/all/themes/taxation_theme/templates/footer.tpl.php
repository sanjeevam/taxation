   <footer id="footer" role="contentinfo" class="clearfix">
                <div class="top-footer"><?php print $footer_message; ?></div>
                <?php if (!empty($footer)): print $footer;
                endif; ?>
                <div class="copyright-message">&copy; 2008 Reed Elsevier Limited. All Rights Reserved.</div>
                <a href="#" class="logo"><img src="<?php echo base_path() . path_to_theme(); ?>/images/adds-lexis-nexis.jpg" alt="Lexis Nexis"></a>
                <?php //print $feed_icons ?>
            </footer> <!-- /#footer -->