<?php
/**
 * @file
 * 
 * WSKAuth to integrate with LN sites.
 * 
 * Written by Dave Brown 2009, db@coolexposure.com / davez1000@gmail.com
 *
 */

define('AUTH_ENABLE', 1);
define('DISPLAY_DEBUGGGING', 0);

include_once drupal_get_path("module","wsk_auth")."/includes/wskfunctions.inc";// 'wskfunctions.inc';

global $user;

/*
 * remember we need to do the IP authentication bit!
 * 
 */
if (isset($_COOKIE['_$subscriber']) && !isset($_COOKIE['_$ipauth']) && !isset($_COOKIE['_$guest']) && !isset($_COOKIE['_$clientadmin']) && !isset($_COOKIE['_$admin'])){
  
  if (DISPLAY_DEBUGGGING) {
  	echo _display_debug_info();
  }
  
//  user_authenticate($_COOKIE['_$userid'], 'zzz');
  
  /*
   * We need to check to see if the user exists in the users table and if not, add them.
   * Plus we need to set their role and if it already exists, update it to make sure it's in sync
   * with the one in the LN system.
   * 
   * If they are IP authenticated, then we need to set them to a dummy user, and show the
   * $acctname or $billgroupname (?)
   */
  
  
  ## THIS PART IS A HACK UNTIL THEY GET THE ADMIN COOKIE SETUP
//  if ($_COOKIE['_$userid'] == 'TAXI9988') {
//    $account = user_load(array(
//      'name'    => 'admin',
//    //'pass'    => 'zzz',
//      'status'  => 1
//      )
//    );
//  }
//  else {
	  $account = user_load(array(
      'name'    => $_COOKIE['_$userid'],
      //'pass'    => 'zzz',
      'status'  => 1
      )
	  );
//  }
  #### FINISH HACK HERE
  
  
  if (!$account) {
  	// the user doesn't exist, so we need to create him with the details from the cookie
//  	echo 'this account does not exist';
  	
    $saveuser = _create_user();
    
  	if ($saveuser) {
      _load_user();
	  	$account = 1;
  	}
  }
  

  
  // if the user exists, then let's load him / log him into drupal
  if(AUTH_ENABLE && $account) {
    $user = $account;
  }
  
  if ($user->uid > 0) {
    echo (DISPLAY_DEBUGGGING)? '<br />now logged into drupal via username: ' . $_COOKIE['_$userid'] : '' ;
  } else {
  	echo (DISPLAY_DEBUGGGING)? '<br />not logged into drupal' : '' ;
  }
  
  
}
else if (isset($_COOKIE['_$guest']) && $user->uid==0) { //CHANGED 17/02
	// logout
	if ($user->uid > 0 && AUTH_ENABLE) {
		require_once drupal_get_path('module', 'user') . '/user.pages.inc';
		user_logout();
	}
	
	$info   = time() . '<br />';
	$info  .= 'guest only<br />';
	
  $thisurl = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  $loginURL = 'http://' . $_COOKIE['_$authserver'] .'/uk/legal/WskAuth/SignIn.aspx?ru=' . urlencode($thisurl);
  
  $info .= '<a href="' . $loginURL . '">sign in</a><br />';
  
  $info .= '<br />' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  echo (DISPLAY_DEBUGGGING)? $info : '' ;
}
else if (isset($_COOKIE['_$admin']) && $_COOKIE['_$admin'] == 't') { ##############
  // log the superadmin in
  $account = user_load(array(
    'name'    => 'admin',
  //'pass'    => 'zzz',
    'status'  => 1
    )
  );
  $user = $account;
}
else if (isset($_COOKIE['_$clientadmin']) && $_COOKIE['_$clientadmin'] == 't') { ##############
  // log the superadmin in
  $account = user_load(array(
    'name'    => 'superadmin',
  //'pass'    => 'zzz',
    'status'  => 1
    )
  );
  $user = $account;
}
else if (isset($_COOKIE['_$ipauth']) && $_COOKIE['_$ipauth'] == 't') { ##############
  // log the superadmin in
  $account = user_load(array(
    'name'    => 'ipauth',
  //'pass'    => 'zzz',
    'status'  => 1
    )
  );
  $user = $account;
}
