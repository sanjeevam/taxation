<?php
function _check_login(){
	if (!isset($_COOKIE['_$guest'])) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

function _build_upgrade_string() {
  $thisurl = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  $upgradeURL = 'http://' . $_COOKIE['_$authserver'] .'/uk/legal/WskAuth/SignIn.aspx?ru=' . urlencode($thisurl);
  return $upgradeURL;
}

function _build_logout_string($r = FALSE) {
  $thisurl = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  if ($r) {
    $thisurl = $r;
  }
  $logoutURL = 'http://' . $_COOKIE['_$authserver'] .'/uk/legal/WskAuth/SignOut.aspx?ru=' . urlencode($thisurl);
  return $logoutURL;
}

function _build_login_string($r = FALSE) {
  $thisurl = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  if ($r) {
  	$thisurl = $r;
  }
  $loginURL = 'http://' . $_COOKIE['_$authserver'] .'/uk/legal/WskAuth/SignIn.aspx?ru=' . urlencode($thisurl);
  return $loginURL;
}

function _build_logout_link() {
  $thisurl = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  $logoutURL = 'http://' . $_COOKIE['_$authserver'] .'/uk/legal/WskAuth/SignOut.aspx?ru=' . urlencode($thisurl);
  $info = '<a href="' . $logoutURL . '">logout</a><br />';
  return $info;
}

function _build_login_link() {
  $thisurl = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  $loginURL = 'http://' . $_COOKIE['_$authserver'] .'/uk/legal/WskAuth/SignIn.aspx?ru=' . urlencode($thisurl);
  $info = '<a href="' . $loginURL . '"><strong>SIGN IN</strong></a><br />';
  return $info;
}

function _display_debug_info() {
  $info  = time() . '<br />';
  $info .= 'logged fs, username: ' . $_COOKIE['_$userid'] . '<br />';
  $info .= 'email: ' . $_COOKIE['_$email'] . '<br />';
  $info .= 'usertypes: ' . $_COOKIE['_$usertypes'] . '<br />';
  
  $thisurl = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  $logoutURL = 'http://' . $_COOKIE['_$authserver'] .'/uk/legal/WskAuth/SignOut.aspx?ru=' . urlencode($thisurl);
  
  $info .= '<a href="' . $logoutURL . '">logout</a><br />';
  return $info;
}

function _create_user() {
  $userdetails = array();
  $userdetails = array(
    'name'    => $_COOKIE['_$userid'],
    'pass'    => '2ty3bqo54arbyjbh',
    'mail'    => (strlen($_COOKIE['$_email']) > 0)? $_COOKIE['$_email'] : 'user@taxation.co.uk' ,
    'init'    => $_COOKIE['_$userid'],
    'status'  => 1,
  );
  $saveuser = user_save('', $userdetails);
  if ($saveuser) {
  	drupal_goto('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
  	return 1;
  }
  else {
  	return 0;
  }
}

function _load_user() {
  $account = user_load(array(
    'name'    => $_COOKIE['_$userid'],
  //'pass'    => 'zzz',
    'status'  => 1
    )
   );
}