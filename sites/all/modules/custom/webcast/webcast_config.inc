<?php
/*
 * Due to this webcast page being hit quite a lot, I decided to get the page config in one query,
 * instead of grabbing it from the DB in each function.
 *
 */

function _get_config() {
  $ret = array();
  $result = db_query("SELECT * FROM {webcast_config}");
  // Count of results so to check file creation or not
  $CNT = db_result(db_query("SELECT count(*) as CNT FROM {webcast_config}"));
  if (intval($CNT) > 0) {
    while ($x = db_fetch_object($result)) {
      switch ($x->name) {
        case 'activate_webcast':
          $ret['wactivate'] = $x->value;
          break;
        case 'disabled_webcast_message':
          $ret['wdisabled'] = $x->value;
          break;
        case 'webcast_title':
          $ret['wtitle'] = $x->value;
          break;
        case 'webcast_blurb';
          $ret['wblurb'] = $x->value;
          break;
        case 'webcast_link':
          $ret['wlink'] = $x->value;
          break;
        case 'webcast_submit_button_text':
          $ret['wsubmit'] = $x->value;
          break;
      }
    }
    return $ret;
  }
}


$wconf = _get_config();

define('WEBCAST_ACTIVATE',  ($wconf['wactivate'] ? $wconf['wactivate'] : 0 ));
define('WEBCAST_DISABLED',  ($wconf['wdisabled'] ? $wconf['wdisabled'] : '' ));
define('WEBCAST_TITLE',     ($wconf['wtitle'] ? $wconf['wtitle'] : '' ));
define('WEBCAST_BLURB',     ($wconf['wblurb'] ? $wconf['wblurb'] : '' ));
define('WEBCAST_LINK',      ($wconf['wlink'] ? $wconf['wlink'] : '' ));
define('WEBCAST_SUBMIT',    ($wconf['wsubmit'] ? $wconf['wsubmit'] : 'Submit your details to see the webcast' ));