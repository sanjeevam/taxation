$(document).ready(function(){
  if ($('#wsk-iframe').length) {
    WSKAUTH.init();
  }
});

var WSKAUTH = {
  container: '#signin-welcome',
  loader: '#loading',
  signIn: '#signin-block',
  url: 'ajax/silent-login',
  delay: 1000,
  attempts: 3,
  running: false,
  
  init: function() {
    this.load();
  },
  reset: function() {
    this.delay = 1000;
    this.attempts = 3;
  },
  load: function() {
    // Don't call if we're already running!
    if(this.running) {
      return;
    }
    this.running = true;
	
    var _wskauth = this;
 
    $.ajax({ 
      type:"get", 
      url: Drupal.settings.basePath + this.url,
      async: false,
	  beforeSend: function() {
        // show the spinner 
        $('#loading').show();
      },
      success: function(data) {
        if(data == 'anon') {
          this.error();
        }
        else {
          this.hideLoading();
          this.hideSignIn();
          $(_wskauth.container).html(data);
        }
      },
      hideLoading: function() {
        // Hide the spinner
        $(_wskauth.loader).fadeOut(2000, 0);
      },
      hideSignIn: function() {
        // Hide the sign in block
        $(_wskauth.signIn).fadeOut(2000, 0);
      },
      complete: function() {
        // reset running variable to false
        _wskauth.running = false;
      },
      error: function(xhr, status) {
        if(_wskauth.attempts-- == 0) {
          // Sorry. We give up.
          this.hideLoading();
          _wskauth.reset();
          return;
        }
        setTimeout(function() {
          _wskauth.load();
        }, _wskauth.delay *= 2);
      }
    });
  }
}
