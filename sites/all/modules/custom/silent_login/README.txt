Silent Login Module
===================

By Gideon Cresswell 2011 for LexisNexis
gideon@gideoncresswell.com

===================

Contents of this folder:

images (directory)
images/loader.gif
js (directory)
js/silent_login.js
DEVELOPER.txt
INSTALL.txt
README.txt (this file)
silent_login.info
silent_login.install
silent_login.module
WEBFORM.txt

Installation instructions and prerequisites are provided in the INSTALL.txt file.

Developer notes are provided in the DEVELOPER.txt containing cookie details, Lexis Library URL parameter meanings.

WEBFORM.txt is the Drupal export of the Terms & Conditions node which must be imported into a site before this module can be installed.

