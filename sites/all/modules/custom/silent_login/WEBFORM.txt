<?php
array(
  array(
    'nid' => '22644',
    'type' => 'webform',
    'language' => 'en',
    'uid' => '1',
    'status' => '1',
    'created' => '1310552859',
    'changed' => '1310552992',
    'comment' => '0',
    'promote' => '1',
    'moderate' => '0',
    'sticky' => '0',
    'tnid' => '0',
    'translate' => '0',
    'premium' => '0',
    'vid' => '22654',
    'revision_uid' => '1',
    'title' => 'Accept terms and conditions',
    'body' => '',
    'teaser' => '',
    'log' => '',
    'revision_timestamp' => '1310552992',
    'format' => '3',
    'name' => 'admin',
    'picture' => '',
    'data' => 'a:2:{s:7:"contact";i:0;s:18:"admin_compact_mode";b:0;}',
    'path' => 'tandc',
    'print_display' => 1,
    'print_display_comment' => 0,
    'print_display_urllist' => 1,
    'webform' => array(
      'nid' => '22644',
      'confirmation' => '',
      'confirmation_format' => '3',
      'redirect_url' => '<confirmation>',
      'status' => '1',
      'block' => '0',
      'teaser' => '0',
      'allow_draft' => '0',
      'auto_save' => '0',
      'submit_notice' => '1',
      'submit_text' => '',
      'submit_limit' => '-1',
      'submit_interval' => '-1',
      'record_exists' => TRUE,
      'roles' => array(
        '0' => '1',
        '1' => '2',
      ),
      'emails' => array(),
      'components' => array(
        '1' => array(
          'nid' => '22644',
          'cid' => '1',
          'form_key' => 'tandc',
          'name' => 'Accept terms and conditions',
          'type' => 'select',
          'value' => '',
          'extra' => array(
            'items' => '1| I acknowledge that I have read and agree to abide by the LexisNexis UK <a href="http://www.lexisnexis.co.uk/onlinetermsconditions">terms &amp; conditions</a>, <a href="http://www.lexisnexis.co.uk/onlinelicence">online license agreement</a> and <a href="http://www.lexisnexis.co.uk/privacypolicy/">privacy policy</a>',
            'multiple' => 1,
            'title_display' => 'before',
            'aslist' => 0,
            'optrand' => 0,
            'conditional_operator' => '=',
            'other_option' => NULL,
            'other_text' => 'Other...',
            'description' => '',
            'custom_keys' => FALSE,
            'options_source' => '',
            'private' => FALSE,
            'conditional_component' => '',
            'conditional_values' => '',
          ),
          'mandatory' => '1',
          'pid' => '0',
          'weight' => '0',
          'page_num' => 1,
        ),
      ),
    ),
    'last_comment_timestamp' => '1310552859',
    'last_comment_name' => NULL,
    'comment_count' => '0',
    'taxonomy' => array(),
    'files' => array(),
    '0' => FALSE,
    'nodewords' => array(),
    'menu' => array(
      'link_title' => '',
      'mlid' => 0,
      'plid' => 0,
      'menu_name' => 'navigation',
      'weight' => 0,
      'options' => array(),
      'module' => 'menu',
      'expanded' => 0,
      'hidden' => 0,
      'has_children' => 0,
      'customized' => 0,
      'parent_depth_limit' => 8,
    ),
    '#_export_node_encode_object' => '1',
  ),
)



-- phpMyAdmin SQL Dump
-- version 3.3.10
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 08, 2011 at 10:55 AM
-- Server version: 5.0.77
-- PHP Version: 5.2.16

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

--
-- Database: `gcresswelltaxation`
--

--
-- Dumping data for table `profile_fields`
--

INSERT INTO `profile_fields` (`fid`, `title`, `name`, `explanation`, `category`, `page`, `type`, `weight`, `required`, `register`, `visibility`, `autocomplete`, `options`) VALUES
(1, 'Accept terms and conditions', 'profile_terms', '<p>\r\n	Accept site terms and conditions.</p>\r\n', 'Terms & Conditions', '', 'checkbox', 0, 1, 1, 1, 0, '');
