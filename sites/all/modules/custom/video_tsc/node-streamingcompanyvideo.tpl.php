<?php
$fullpath = 'http://' . $_SERVER['HTTP_HOST'] . base_path();
drupal_add_js(drupal_get_path('module','video_tsc').'/js/swfobject.js', 'theme');

global $user;
if ($user->uid < 1 && $node->field_freetoair_version[0]['value']) {
  $filename = preg_replace('/(.+)\.([^\.]+)$/i', '\\1_free.\\2', $node->field_videofilename[0]['value']);
}
else {
  $filename = $node->field_videofilename[0]['value'];
}
if ($node->field_isaudio[0]['value']) {
  $vheight = 100;
}
else {
  $vheight = 253;
}
$acc_no = $node->field_accountid[0]['value'];
if (trim($acc_no) == '')
  $acc_no = variable_get('video_tsc_default_accno','');
?>

<script type="text/javascript">

	// Setup for 1st Video
	// serverpath allow us to locate the player.swf and support files elsewhere
		var serverPath = "<?php print url(drupal_get_path('module','video_tsc').'/tsc/'); ?>";
		var paths = "http://hwcdn.net/<?php echo $acc_no; ?>/fms/<?php echo preg_replace('/(.+)\.([^\.]+)$/i', '\\1_low.\\2', $filename); ?>.smil^LOW^false<?php if ($node->field_defaultquality[0]['value'] == 'LOW') { echo '^default'; } ?>"
					+ ",http://hwcdn.net/<?php echo $acc_no; ?>/fms/<?php echo $filename; ?>.smil^MEDIUM^false<?php if ($node->field_defaultquality[0]['value'] == 'MEDIUM') { echo '^default'; } ?>"
					+ ",http://hwcdn.net/<?php echo $acc_no; ?>/fms/<?php echo preg_replace('/(.+)\.([^\.]+)$/i', '\\1_high.\\2', $filename); ?>.smil^HIGH^false<?php if ($node->field_defaultquality[0]['value'] == 'HIGH') { echo '^default'; } ?>";


	// buf must be an integer
	// ap = "autoplay" on start
	// vi = initial image
	var flashvars = {
    <?php if ($node->field_autoplay[0]['value']) { echo 'ap: true,'; } ?>
	  vp: paths,
    <?php if ($node->field_videoimage[0]['filepath']): ?>
      vi: "<?php echo $fullpath . $node->field_videoimage[0]['filepath']; ?>",
    <?php else: ?>
      vi: "<?php echo $fullpath; ?>tsc/initial.jpg",
    <?php endif; ?>
	  buf: '<?php echo $node->field_buffer[0]['value']; ?>',
	  link: "<?php echo $node->field_sitelink[0]['value']; ?>",
	  css: serverPath
	};
	var params = { allowscriptaccess: 'always', allowfullscreen: 'true', bgcolor:'#000000' };
	swfobject.embedSWF("<?php print url(drupal_get_path('module','video_tsc').'/tsc/'); ?>player.swf", "myContent", "450", "<?php echo $vheight; ?>", "9.0.115", "<?php print url(drupal_get_path('module','video_tsc').'/tsc/'); ?>expressInstall.swf", flashvars, params);
</script>
<div id="myContent">
<h2>Alternative content</h2>
<p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
</div>
